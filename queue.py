import datetime

class queueTask:
    """Defines a task in a queue"""

    priority = 0        #Priority, Integer
    command = None      #The task itself
    args = ()
    predecessor = None  #A task that HAS to be completed before
                        #proceeding to this one. Defaults to `None`

    #A time interval in which the task shuld be done, otherwise it is
    #cancelled
    timeWindowBefore = None
    timeWindowAfter  = None

    ## Metadata
    owner = None        #Who issued the task
    issuedTime = None   #When was the task issued
    completed = False   #Whether the task is completed
    queue = None        #A task can be present in max one queue
    estimatedTime = 0   #Time needed for completition

    def __init__(self, command, args, priority, owner, predecessor=None,
                                after=datetime.datetime.now(), before=None):
        """Create a new task"""

        self.command = command
        self.args = args
        self.owner = owner
        self.predecessor = self.predecessor
        self.issuedTime = datetime.datetime.now()
        self.timeWindowAfter = after
        self.timeWindowBefore = before

    def do(self):
        """Perform the task, and inform its owner when done"""

        self.command(*self.args)
        self.owner.taskCompleted(self)
        self.completed = True

    def isValid(self):
        """Whether the task can be performed at this moment"""

        #If set, is the predecessor task done?
        if self.predecessor is not None:
            if not self.predecessor.completed:
                return False

        #Is it too early to commit the task?
        if self.timeWindowAfter is not None:
            if self.timeWindowAfter >= datetime.datetime.now():
                print "too early"
                return False

        #Is it too late? If so, cancel the task
        if self.timeWindowBefore is not None:
            if self.timeWindowBefore < datetime.datetime.now():
                if self.queue is not None:
                    self.owner.taskCancelled(self)
                    self.queue.dequeueTask(self)
                    #TODO: Cancel dependencies too
                return False

        return True

    def timeEllapsed(self):
        """Return time ellapsed since `timeWindowAfter`"""
        return datetime.datetime.now() - self.timeWindowAfter


class queueManager:
    """Defines a queue of tasks"""

    def __init__(self, tasks=[]):
        self.queue = tasks

    def queueTask(self, task):
        """Add a task to the queue"""
        task.queue = self
        self.queue += [task]

        #TODO: queueTask w/ task arguments

    def dequeueTask(self, task):
        """Remove a task from the queue"""
        while task in self.queue: self.queue.remove(task)

    def chooseTask(self):
        """Returns the top-priority task"""
        #Look only at the tasks that can be perfomed at the moment
        tasks = [task for task in self.queue if task.isValid()]
        if len(tasks) == 0:
            return None
        #Sort by: 1)issuer priority; 2) task priority and 3) task age
        tasks.sort(key=lambda t:(t.owner.priority, t.priority, t.timeEllapsed))
        #Perform the task with highest priority
        tasks[-1].do()
        self.dequeueTask(tasks[-1])

#TODO: Assign queueManager automatically to peripherals
