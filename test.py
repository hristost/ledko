import smbus
import i2cDevice
import servo

bus = smbus.SMBus(1)
controller = i2cDevice.controller_i2c(bus, 0x04)
servo_hand_right_pan = servo.servoMotor_i2c(controller, 6)
servo_hand_right_pan.move(90)
