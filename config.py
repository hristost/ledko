import queue
from programs.program       import *

from peripherals            import *
from peripherals.camera     import *
from peripherals.controller import *
from peripherals.motor      import *

import time

#EXAMPLE CONFIGURATION:
#The robot waves when it sees a face

## DEFINE PERIPHERALS
camera = Camera_raspi()

##On a real robot, we  use
i2cBus = smbus.SMBus(1)
controller = Controller_i2c(i2cBus, 0x04)

# Hand servos
servo_hand_right_pan    = servo.servoMotor_i2c(controller, 6)
servo_hand_right_tilt   = servo.servoMotor_i2c(controller, 5)
servo_hand_left_tilt    = servo.servoMotor_i2c(controller, 3)
servo_hand_left_pan     = servo.servoMotor_i2c(controller, 2)

servo_hand_right_pan.controlRange   = (135, 33)
servo_hand_right_tilt.controlRange  = (12, 170)
servo_hand_left_pan.controlRange    = (141, 45)
servo_hand_left_tilt.controlRange   = (160, 10)

servo_hand_right_pan.attach()
servo_hand_right_tilt.attach()
servo_hand_left_tilt.attach()
servo_hand_left_pan.attach()

# Head servos
servo_head_pan     = servo.servoMotor_i2c(controller, 4)
#servo_head_pan.attach()


peripheralDevices = [camera, servo_hand_right_pan, servo_hand_right_tilt, servo_hand_left_tilt, servo_hand_left_pan]

##DEFINE PROGRAMS

#We usually do this in seperate files in the `programs` folder, but this
# one is short:
class prg_findFace(programModule):
    moduleName = "Follow faces"
    priority = 0
    def __init__(self, camera, panServo = None, tiltServo = None):
        super(prg_findFace, self).__init__()
        print "newVision"

        self.cam = camera
        self.cam.open()
        self.handPanServo = panServo
        self.handTiltServo = tiltServo


    def run(self):
        img = self.cam.getImage()
        # Look for a face
        faces = img.findHaarFeatures('face.xml')
        if len(faces) >= 1:
            #If a face is found, wave wi
            self.handTiltServo.move(135)
            for i in range(0, 3):
                for m in range(0, 30):
                    self.handPanServo.move(90-m)
                    time.sleep(0.01)

                for m in range(0, 30):
                    self.handPanServo.move(90-30+m)
                    time.sleep(0.01)
            #TODO: Use the queue



programs = [prg_findFace(camera, servo_hand_right_pan, servo_hand_right_tilt)]
