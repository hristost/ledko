import rpyc
import queue
from programs.program       import *
class program:
    queue = None
    priority = 0
    def __init__(self):
        pass
    def taskCancelled(self, task):
        pass
    def taskCompleted(self, task):
        pass
    def run(self):
        """Main loop"""
        pass

def p(x):
	print x

t = queue.queueTask(p, ["Hello"], 0, program())

c = rpyc.connect("localhost", 18861, config = {"allow_public_attrs" : True, "allow_setattr": True})

c.root.queueTask(t)
print c.root.getQueue()
