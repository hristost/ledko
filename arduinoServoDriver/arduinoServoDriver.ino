#include <Wire.h>
#include <Servo.h>

#define SLAVE_ADDRESS 0x04


int number = 0;
int state = 0;

Servo servos[12];

const byte CMD_ATTACH = 1;
const byte CMD_DETACH = 2;
const byte CMD_MOVE = 3;

void setup() {
    pinMode(13, OUTPUT);
    // initialize i2c as slave
    Wire.begin(SLAVE_ADDRESS);

    // define callbacks for i2c communication
    Wire.onReceive(receiveData);
    Wire.onRequest(sendData);

}

void loop() {
    delay(100);
}

// callback for received data
int writeCommand = -1;
void receiveData(int byteCount){

    while(Wire.available()) {
        digitalWrite(13, HIGH);
        byte received = Wire.read();
        if (writeCommand){
          servos[writeCommand].write(received);
          writeCommand = -1;
        }

        byte command = received & B00001111;
        byte pin = received >> 4;

        switch(command){
            case CMD_ATTACH:
                servos[pin].attach(pin);
                break;

            case CMD_DETACH:
                servos[pin].detach();
                break;

            case CMD_MOVE:
          writeCommand = pin;
            break;
        }
     }
     digitalWrite(13, LOW);
}

// callback for sending data
void sendData(){
    Wire.write(number);
}
