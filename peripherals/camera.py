import os
import subprocess
import SimpleCV

class Camera:
    """Camera object template"""
    def __init__(self):
        pass
    def open(self):
        """Open connection with the camera"""
        pass
    def close(self):
        """Close connection with the camera"""
        pass
    def getImage(self):
        """Capture and return the frame"""
        pass


class Camera_usb(Camera):
    def __init__(self, i=0):
        self.cameraId = i
    def open(self):
        self.cvcam = SimpleCV.Camera(self.cameraId, {"width": 480, "height": 360})
    def close(self):
        self.cvcam = None
    def getImage(self):
        return self.cvcam.getImage()

class Camera_raspi(Camera):
    """Camera connected via a CSI port"""

    scriptsPath = "scripts" #Where `raspifastcamd` is located
    imageNumber = -1        #The script saves the imaages in sequential
                            #filenames, this is the last number
    def __init__(self, i=0):
        self.cameraId = i
    def open(self):
    	pass
    def close(self):
    	pass

    def getImage(self):
        subprocess.call("raspistill -w 320 -h 240 -vf -n -t 1 -o img.jpg", shell=True)
	#self.capture()             #Capture new frame
    	#file = self.getFilename()  #Get the frame's filepath
        img = SimpleCV.Image("img.jpg") #Create a SimpleCV Image
    	#subprocess.call("rm %s" % file, shell=True) #Delete the file
    	return img
