
from math import *

class Servo:
    """Servo motor object template"""
    servoRange = (0, 180)       #Permitted rotation range
    position = 90               #Current servo position
    controlRange = servoRange   #Map normalised position to absolute
    attached = False            #Whether the servo is active

    def __init__(self):
        pass

    def mapPosition(x):
        """Map a value in [-1, 1] to controlRange"""
        (a, b) = controlRange
        return float(a)+float(b-a)*(x+1.0)/2.0

    def move(self,newPosition):
        """Move the motor to a new absolute position."""
        #Make sure `newPosition` is in bounds
        newPosition = max(newPosition, self.servoRange[0])
        newPosition = min(newPosition, self.servoRange[1])
        #Attach servo if not attached already:
        if not self.attached:
            self.attach()
        #Move
        self.position = newPosition
        self.sendData(newPosition)

    def moveMapped(self, newPosition):
        """Move the motor to a new position, ranging in [-1, 1]."""
        self.move(self.mapPosition(newPosition))

    def moveRelative(self, difference):
        """Move the motor to a new relative position."""
        self.move(self.position + difference)



class Servo_dummy(Servo):
    """Simulates a servo by printing position to the console"""

    def __init__(self):
        print "Init " + str(self)
        #Create a window for visualising the servo
        self.window = pyglet.window.Window(128,196,
            style=pyglet.window.Window.WINDOW_STYLE_TOOL,
            caption='Servo')
        @self.window.event
        def on_draw():
            self.window.clear()
            pyglet.gl.glColor3f(0.2,0.2,0.2)
            drawRect(16, 16, 96, 164)
            pyglet.gl.glColor3f(1,1,1)
            drawCircle(64,196-64,48)
            arrowPoints = ((0, -3), (48, -3), (48, 3), (0, 3))
            radians = float(360-self.position)/360.0*2*pi
            rotated = map(lambda p: (64+float(p[0]) * cos(radians) + float(p[1]) * sin(radians),196-64+float(p[1]) * cos(radians) - float(p[0]) * sin(radians)), arrowPoints)
            points = [int(e) for l in rotated for e in l]
            pyglet.gl.glColor3f(1,0.2,0.2)
            pyglet.graphics.draw(4, pyglet.gl.GL_POLYGON,
                ('v2i', points))

        tick()
        tick()


    def attach(self):
        print "Attach " + str(self)


    def detach(self):
        print "Detach " + str(self)
        pygame.display.set_caption('Servo -- Detached')

    def sendData(self, position):
        print "Move " + str(position) + " " + str(self)
        tick()





# Servo connected to a controller

#Protocol commands
I2C_SERVO_CONTROLLER_ATTACH = 1
I2C_SERVO_CONTROLLER_DETACH = 2
I2C_SERVO_CONTROLLER_MOVE = 3

class Servo_controller(Servo):
    """Servo connected to a controller"""
    #Circuit:
    #   Microcontroller w/ the firmware connected via i2c
    #   Servo connected to a microcontroller output

    def __init__(self, device, pin, enabledRange = (0, 180)):
        self.device = device
        self.servoPin = pin
        self.servoRange = enabledRange
    def attach(self):
        self.device.send((self.servoPin<<4) | I2C_SERVO_CONTROLLER_ATTACH)
        self.attached = True
    def detach(self):
        self.device.send((self.servoPin<<4) | I2C_SERVO_CONTROLLER_DETACH)
        self.attached = False
    def sendData(self, position):
        try:
            self.device.send((self.servoPin<<4) | I2C_SERVO_CONTROLLER_MOVE)
            self.device.send(position)
        except:
            print "i2c message lost :(("

#TODO: PWM Servo, Servo w/driver



## Dummy drawing
try:
    import pyglet
except ImportError:
    pass

def tick():
    pyglet.clock.tick()
    for window in pyglet.app.windows:
            window.switch_to()
            window.dispatch_events()
            window.dispatch_event('on_draw')
            window.flip()

def drawRect(x, y, w, h):
    pass

    pyglet.graphics.draw(4, pyglet.gl.GL_POLYGON,
                ('v2i', (x, y, x+w, y, x+w, y+h, x, y+h)))
def rotatePoint(point, origin, radians):
    displacement = point - origin
    point.x = displacement.x * cos(radians) + displacement.y * sin(radians)
    point.y = displacement.y * cos(radians) - displacement.x * sin(radians)

def drawCircle(x, y, radius):
    pi = 3.1415926535897932384626
    iterations = int(2*radius*pi)
    s = sin(2*pi / iterations)
    c = cos(2*pi / iterations)

    dx, dy = radius, 0

    pyglet.gl.glBegin(pyglet.gl.GL_TRIANGLE_FAN)
    pyglet.gl.glVertex2f(x, y)
    for i in range(iterations+1):
        pyglet.gl.glVertex2f(x+dx, y+dy)
        dx, dy = (dx*c - dy*s), (dy*c + dx*s)
    pyglet.gl.glEnd()
