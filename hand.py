class hand:
    panPosition = 90
    tiltPosition = 90
    def __init__(self, servo_pan, servo_tilt):
        self.servo_pan = servo_pan
        self.servo_tilt = servo_tilt
    def point(self, x, y):
        self.servo_pan.move(x)
        self.servo_tilt.move(y)
