import queue
import config #contains configuration & programs


for device in config.peripheralDevices:
    device.queueManager = queue.queueManager()

while True:
    for program in config.programs:
        program.run()
        #TODO: Use real threading

    for device in config.peripheralDevices:
        device.queueManager.chooseTask()

