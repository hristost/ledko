from peripherals.camera import *
from SimpleCV import Image

cam = Camera_usb()
cam.open()
disp = SimpleCV.Display(cam.getImage().size())
while disp.isNotDone():
    #loads one of the built-in Haar Cascade files
    img = cam.getImage()
    # Look for a face
    faces = img.findHaarFeatures('face.xml')

    for face in faces:
        face.draw()

    img.save(disp)



