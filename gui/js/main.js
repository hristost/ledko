var stage;
var simulation={active:false};
var temporaryWire;
var dragging;
var objects = {};
var wires = {};
var selectedWires={};
var selectedObjects={};
var update = false;
var connecting={active: false};
var connectingStartPoint = {top: 0, left:0};
var connectingStartPointBezierControl = {top: 0, left:0};
var connectingAnchor;

var selectionShadow=new createjs.Shadow("#ffffff", 0, 0, 10);
var objectShadow=new createjs.Shadow("#000000", 0, 1, 10);

var templates = new Array();

window.addEventListener('resize', resize, false);

var editingUI=["backLink", "newFileButton", "openFileButton", "saveFileButton", "componentsPanel"];

var templates = ["a"];


Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function init() {
	// Интерфейсът използва прозорци от `bootstrap` библиотеката
	$('[data-toggle="popover"]').popover()

	// Всички компоненти от панела могат да се влачат до работната площ (#circuitCanvas)
	$("#objectPalette li").draggable({
	    helper: 'clone'
	});
	$("#circuitCanvas").droppable({
	    accept: "li",
	    drop: function(event,ui){
	       	initObject($(ui.draggable).data("object"), {left:event.clientX, top:event.clientY}, false);
	    }
	});
	
	initKeys();
	
	stage = new createjs.Stage("circuitCanvas");	
	stage.enableMouseOver(20);
	stage.mouseMoveOutside = true; // keep tracking the mouse even when it leaves the canvas
	createjs.Touch.enable(stage);
	
	console.log("loadComponents");
	loadComponents();
	console.log(componentTemplates["MCU"]);

	templates = componentTemplates;
	temporaryWire = new createjs.Shape();
	stage.addChild(temporaryWire);
	
	update=true;

	resize();
	createjs.Ticker.addEventListener("tick", tick);
	
	$("#openModal").on('show.bs.modal', listFiles);

}

/// Добавя компонент от тип `templateName` в работното пространство
function addComponentFromTemplate(templateName){
	var component = componentTemplates[templateName];

	// Намери уникално име на компонента
	var componentId=1;
	while(objects.hasOwnProperty(templateName+componentId))componentId++;
	name=templateName+componentId;

	
	component.name = name;
	component.parameterValues.variableName = name;

	addComponentToStage(component);
	
	objects[name] = component
	return name;
}

function addComponentToStage(component){
	name = component.name;
	// Добави кода на компонента
	$("#frame").append(createComponentHTML(component));
	
	// Взаимодействие с потребителя
	var objectClickFunction = function(event){
		objectName = event.data.objectName;
		console.log(objectName)
		selected=!selectedObjects[objectName];
		if(!shiftDown)
			deselectAll();
	
		selectedObjects[objectName]=selected;
		
		if(selectedObjectsCount()==0){
			hideSelectionPanel();
		}

		if(selectedObjects[objectName]){
			$("#frame #"+objectName).addClass("selected");
		}else{
			$("#frame #"+objectName).removeClass("selected");
		}
		update=true;
				
	}
	var dragObjectFunction = function(event, ui){
		var objName = ui.helper.attr('id');
		console.log("wires " + wires.length)

		for (var i in wires) {
			wire = wires[i];
			if (wire.fromName == objName || wire.toName == objName)
				wire.update = true;		
		}

		drawWires();
		stage.update();
	}
	var outletStartDragFunction = function(event, ui){
		connectingAnchor = ui.helper
		for (var i in wires) {
			wire = wires[i];
			var componentName = getOutletComponent(connectingAnchor).attr("id")
			var componentOutlet = connectingAnchor.attr("data-outlet")
			console.log(componentName+">"+componentOutlet)
			if (wire.fromName == componentName && wire.fromOutlet == componentOutlet){
				connectingAnchor = $("#"+wire.toName+' td[data-outlet="'+wire.toOutlet+'"]')
				deleteWire(i);
			}else if (wire.toName == componentName && wire.toOutlet == componentOutlet){
				connectingAnchor = $("#"+wire.fromName+' td[data-outlet="'+wire.fromOutlet+'"]')
				deleteWire(i);
			}
		}

		var connectingAnchorAbsolutePosition = connectingAnchor.offset();
		ay= connectingAnchorAbsolutePosition.top+ connectingAnchor.height()/2
		ax= connectingAnchorAbsolutePosition.left + (connectingAnchor.hasClass("right") ? connectingAnchor.width() : 0)
		connectingStartPoint = {top: ay, left: ax}
	}
	var outletDragFunction = function( event, ui ){
		ui.position = {top: 0, left: 0};
		$("#frame .component").each(function(index){
			var obj = $(this)
			if(!obj.is(ui.helper.parent())){
			if(obj.hitTestPoint({"x":event.clientX, "y":event.clientY, "transparency":false})){
				obj.addClass("destination");
				var sourceType = connectingAnchor.attr("data-outlet-type");
				obj.find("td[data-outlet]").removeClass("available");

				var validOutlets = getValidConnectionsForType(sourceType);
				for(type in validOutlets){
				console.log(validOutlets[type]);

					obj.find('td[data-outlet-type="'+validOutlets[type]+'"]').addClass("available");
				}
			}else{
				obj.removeClass("destination");
			}
			}
		})
		var parentPos = $("#frame").offset();
		var ax = event.clientX - parentPos.left
		var ay = event.clientY - parentPos.top
		var bx = connectingStartPoint.left - parentPos.left
		var by = connectingStartPoint.top - parentPos.top
		var bbx = bx + (connectingAnchor.hasClass("right") ? 50 : -50)
		var bby = by
		console.log("draw line")
		temporaryWire.graphics.clear().setStrokeStyle(2, 'round', 'round').beginStroke("#333333").moveTo(bx, by).curveTo(bbx, bby, ax, ay);

		//temporaryWire.graphics.clear().setStrokeStyle(2, 'round', 'round').beginStroke("#333333").moveTo(ax, ay).bezierCurveTo(abx, aby, bbx, bby, bx, by);
		update = true;
		stage.update()

	}
	var outletEndDragFunction = function(event, ui){
		console.log("stop")
		temporaryWire.graphics.clear()
		$("#frame .component td[data-outlet].available").each(function(index){
			var obj = $(this)
			if(!obj.is(ui.helper.parent())){
			if(obj.hitTestPoint({"x":event.clientX, "y":event.clientY, "transparency":false})){
				console.log("conn")
									console.log("connA")

					var toOutlet = obj.attr("data-outlet")
					var toComponent = getOutletComponent(obj).attr('id')
					var fromOutlet = connectingAnchor.attr("data-outlet")
					var fromComponent = getOutletComponent(connectingAnchor).attr('id')
					console.log(toComponent+">"+toOutlet+" "+fromComponent+">"+fromOutlet)
					initWire(fromComponent, fromOutlet, toComponent, toOutlet)
					drawWires()
				
			}
			}
		})
		update=true
		stage.update()

	}
	$("#frame #"+name+" td[data-outlet]").draggable({
		revert: true,
		revertDuration: 0,
		drag: outletDragFunction,
		start: outletStartDragFunction,
		stop: outletEndDragFunction,
	});

	//
	$("#frame #"+name+" td[data-outlet]").droppable({
	    hoverClass: "hover",
	    accept: "td[data-outlet]"
	});
	$("#frame #"+name).draggable({
		drag: dragObjectFunction
	});
	$("#frame #"+name+" table").click({
		objectName: name
	}, objectClickFunction);
}
function openParametersPane(objectName){
	$("#"+objectName+" div.parameterPane").html(createComponentSettingsPanelHTML(objects[objectName]));
	$("#"+objectName+" footer").hide(500);
	$("#"+objectName+" div.parameterPane").show(500)
}
function closeParametersPane(objectName){
	$("#"+objectName+" div.parameterPane").hide(500)
	$("#"+objectName+" footer").show(500);
}
function submitParametersPane(objectName){
	var component = objects[objectName]
	$("#"+objectName+" input[data-bound-parameter]").each(function(index){
		console.log(this)
		var obj = $(this);
		var objectName = obj.attr("data-bound-object")
		var paramName = obj.attr("data-bound-parameter")
		objects[objectName].parameterValues[paramName] = obj.val();
	})
	$("#"+objectName+" span[data-variable]").html(component.titleFunction(component))
	closeParametersPane(objectName)
}

/// Масив с позволените връзки за извод от тип `type`
function getValidConnectionsForType(type){
	var validConnections = {'IO':['I', 'O', 'IO'], 'I': ['O', 'IO'], 'O': ['I', 'IO']}
	if (validConnections.hasOwnProperty(type)){
		return validConnections[type];
	}else{
		return [type];
	}
}

/// Името на компонента, в който се намира jQuery обектът `outlet`
function getOutletComponent(outlet){
	var parents = outlet.parentsUntil("#frame")
	return $(parents[parents.length - 1])
}

function deselectAll(){
	console.log("deselectAll");
	for (var i in selectedObjects) {
		selectedObjects[i]=false;
	}
	for (var i in selectedWires) {
		selectedWires[i]=false;
	}
	for (var i in wires) {
		wires[i].shape.shadow = null;
	}
	for (var i in objects) {
		$("#frame #"+i).removeClass("selected")
	}
	update=true;
	
}
function selectedObjectsCount(){
	count=0
	for(var id in selectedObjects)
	if(selectedObjects[id])
	for(var id in selectedWires)
	if(selectedWires[id])
	count++
	return count
}
function hideSelectionPanel(){
	shiftDown=false;
	$("#shiftButton").removeClass("active");
	$("#selectionPanel").hide("blind", 500);
}

/// Изчертава връзките м/у модулите
function drawWires() {
	for (var i in wires) {
		console.log("Drawing wire " + i);
		wire = wires[i];
		if (wire.update) {
			//Намери позицията на изводите в canvas елемента
			var parentPos = $("#frame").offset();

			//Позицията на първия извод
			var outlet = $("#frame #"+wire.fromName+' td[data-outlet="'+wire.fromOutlet+'"]');
			var outletAbsolutePosition = outlet.offset();
			ay= outletAbsolutePosition.top - parentPos.top + outlet.height()/2
			ax= outletAbsolutePosition.left - parentPos.left + (outlet.hasClass("right") ? outlet.width() : 0)
			abx = ax + (outlet.hasClass("right") ? 40 : -40);
			aby = ay;
			
			//Позицията на втория извод
			outlet = $("#frame #"+wire.toName+' td[data-outlet="'+wire.toOutlet+'"]');
			outletAbsolutePosition = outlet.offset();
			by= outletAbsolutePosition.top - parentPos.top + outlet.height()/2
			bx= outletAbsolutePosition.left - parentPos.left + (outlet.hasClass("right") ? outlet.width() : 0)
			bbx = bx + (outlet.hasClass("right") ? 40 : -40);
			bby = by;
			
			wire.shape.graphics.clear().setStrokeStyle(2, 'round', 'round').beginStroke("#333333").moveTo(ax, ay).bezierCurveTo(abx, aby, bbx, bby, bx, by);
			wire.update = false;
		}
	}

	// Означи изводите като свързани
	drawOutlets();
}

function drawOutlets(){
	$("#frame td[data-outlet]").removeClass("connected");
	for (var i in wires) {
		console.log("Drawing wire " + i);
		wire = wires[i];
		$("#frame #"+wire.fromName+' td[data-outlet="'+wire.fromOutlet+'"]').addClass("connected")
		$("#frame #"+wire.toName+' td[data-outlet="'+wire.toOutlet+'"]').addClass("connected")
	}
}
function resize() {
	stage.canvas.width = $("#frame").width();
	stage.canvas.height = $("#frame").height();
	stage.onMouseDown = function(evt) {
	    alert("the canvas was clicked at "+evt.stageX+","+evt.stageY);
	}
	stage.onMouseMove = function(evt) {
	    console.log("stageX/Y: "+evt.stageX+","+evt.stageY); // always in bounds
	    console.log("rawX/Y: "+evt.rawX+","+evt.rawY); // could be < 0, or > width/height
	}
	stage.update();
}

function tick(event) {
	// this set makes it so the stage only re-renders when an event handler indicates a change has happened.
	if (update) {
		drawWires();
		update = false; // only update once
		stage.update(event);
	}
}

var shiftDown = false;

function initKeys(){
	
	var keyDown = function(event){
	    if(event.keyCode === 16 || event.charCode === 16){
	        shiftDown = true;
			$("#shiftButton").addClass("active");
	    }
	    if(event.keyCode === 8 || event.charCode === 8 || event.keyCode === 46 || event.charCode === 46){
			console.log("delete");
			deleteSelection();
	    }
	};

	var keyUp = function(event){
	    if(event.keyCode === 16 || event.charCode === 16){
	        shiftDown = false;
			$("#shiftButton").removeClass("active");
	    }
	};

	window.addEventListener? document.addEventListener('keydown', keyDown) : document.attachEvent('keydown', keyDown);
	window.addEventListener? document.addEventListener('keyup', keyUp) : document.attachEvent('keyup', keyUp);
}
function toggleShift(){
	//Променливата shiftDown показва дали е натиснат клавишът shift, за да се избере повече от един обект
	//На мобилни устройства без клавиатура, shiftButton се появява и замества физическия клавиш shift
	shiftDown=!shiftDown;
	if (shiftDown)
		$("#shiftButton").addClass("active");
	else
		$("#shiftButton").removeClass("active");
}
function initWire(fromName, fromOutlet, toName, toOutlet){
	//Прави нова връзка
	//1)провери дали такава връзка вече съществува
	
	var valid=true;
	if(fromName==toName&&fromOutlet==toOutlet)valid=false;
	for (var w in wires){
		if(wires[w].fromName==fromName && wires[w].toName==toName && wires[w].fromOutlet==fromOutlet && wires[w].toOutlet==toOutlet)valid=false;
		
		if(wires[w].toName==fromName && wires[w].fromName==toName && wires[w].toOutlet==fromOutlet && wires[w].fromOutlet==toOutlet)valid=false;	
	} 
	
	if(valid){
		//1)Намери име на връзката
		newWireId=1;
		while(wires.hasOwnProperty("WIRE"+newWireId))newWireId++;
		name="WIRE"+newWireId;
		newWire={	fromName: 	fromName,
					fromOutlet: fromOutlet,
					toName: 	toName,
					toOutlet: 	toOutlet,
					update: true};
	object = new createjs.Shape();
	object.name = name;
	object.on("click", function(evt) {
		
			selected=!selectedWires[this.name];
			if(!shiftDown)deselectAll();
		selectedWires[this.name]=selected;
		
		if(selectedObjectsCount()==0){
			shiftDown=false;
			$("#shiftButton").removeClass("active");
			$("#selectionPanel").hide("blind", 500);
		}
		if(selected)
		$("#selectionPanel").show("blind", 500);
		//
		if(selectedWires[this.name])
		this.shadow=selectionShadow;
		else
		this.shadow = null;
		
		
		update=true;
	
	});
	
	stage.addChild(object);
	newWire.shape=object;
	wires[name]=newWire;
	saved=false;
	}
}
function initObject(type, position, locked){
	// TODO: Remove `locked`
	var name = addComponentFromTemplate(type)

	var objectJQ = $("#"+name)
	var frameJQ = $("#frame")


	objectJQ.offset(position)
}

function deleteSelection(){
    for (var i in wires) 
		if(selectedWires[i])
			deleteWire(i);
			
    for (var i in objects) 
		if(selectedObjects[i]){
			console.log("delete ", i);
	
			$("#frame #"+i).remove()
			for (var w in wires){
				if(wires[w].fromName==i || wires[w].toName==i){
					deleteWire(w);
				}
			} 
			
			delete objects[i];
		}
	update=true;
	saved=false;
	hideSelectionPanel();
}
function deleteWire(w){
	stage.removeChild(wires[w].shape);
	delete wires[w];
	selectedWires[w]=false;
	delete selectedWires[w];
	saved=false;
}

//Зареждане и запазване на файлове

newFile = true
saved = true
openedFilename = ""

function createNewFile(){
	if(!saved)
			if(!confirm("Вашата конфигурация не е запазена. Сигурни ли сте, че искате да създадете нова?"))
				return
	newFile=true
	saved= true
	clearCanvas()
	update=true		
}
function openFileDialog(){
	$('#openModal').modal()
}
function showCode(){
	$("#codeModal").modal()
	$("#codeModal code").html("")

	for (objName in objects){
		var obj = objects[objName]
		var func = codeGenerationFunctions[obj.type]
		if (!(typeof func === "undefined")){
				$("#codeModal code").append(func(obj))
				$("#codeModal code").append("<br>")

		}
	}
	$('code').each(function(i, block) {
    	hljs.highlightBlock(block);
  });
}
function saveFileDialog(){
	if (newFile){
		
		var newName = prompt("Въведете име на новата верига", "");


		if (newName !== null) {  
			if(!(/^\s*$/.test(newName))) {
				willsave=true;
				if (nameIsTaken(newName)){
					willsave=confirm("Конфигурация с такова име вече съществува. Да съхраня ли новата конфигурация върху нея?");
				}   
				if(willsave){                                      
					newFile=false;
					openedFilename=newName
					saveCircuit(openedFilename)   
				}
			}                  
		}
		
	}else{
		saveCircuit(openedFilename)
	}
}

function listFiles(){
	tableStr="";
	if(localStorage["files"]){
		fileArray = JSON.parse(localStorage["files"]);
		for (var i in fileArray){
			tableStr=tableStr+'<tr><th onclick="loadCircuitFromName('+"'"+i+"'"+')"">'+fileArray[i].title+'</th><td><a onclick="deleteCircuit('+"'"+i+"'"+', '+"'"+fileArray[i].title+"'"+')"><span class="glyphicon glyphicon-trash"></span></a></td></tr>';
		}
	}
	if (tableStr=="")tableStr='<tr><td>Няма запазени конфигурации. Можете да запишете конфигурация с бутона [ <span class="glyphicon glyphicon-floppy-disk"></span> ].</td></tr>'
	$("#fileList").html(tableStr);
}
function saveCircuit(title){
	//Запази веригата с име filename
	fileContents={};
	//Впиши всички 
	for (var i in objects){
		obj=objects[i];
		fileContents[i]={type: obj.type, position: getComponentPosition(obj), objectParams: obj.parameterValues};
	}
	for (var i in wires){
		wire=wires[i];
		fileContents[i]={type:"WIRE",fromName: wire.fromName, fromOutlet: wire.fromOutlet, toName: wire.toName, toOutlet: 	wire.toOutlet};
	}
	file = {title: title, contents: fileContents}
	if (localStorage["files"])
		fileArrays=JSON.parse(localStorage["files"]);
	else
		fileArrays={}
	fileArrays["file_"+title]=file;
	
	localStorage["files"]=JSON.stringify(fileArrays)
	
	saved=true
	
	$("#saveFileButton span").addTemporaryClass("glyphicon-floppy-saved", 2000);
}
function loadCircuitFromName(filename){
	$('#openModal').modal("hide");
	if(!saved)
		if(!confirm("Вашата конфигурация не е запазена. Сигурни ли сте, че искате да заредите нова?"))
			return
	
	fileArray=JSON.parse(localStorage["files"]);
	file=fileArray[filename]
	
	clearCanvas();
	//load file

	newObjects=file["contents"];
	newFile=false
	openedFilename=file["title"]
	for (var i in newObjects){
		var obj=newObjects[i]
		if (obj.type!="WIRE"){
			var object = componentTemplates[obj.type]
			object.parameterValues = obj.objectParams
			object.name = i
			addComponentToStage(object)
			$("#frame #"+i).offset(obj.position)
			objects[i] = object
			//insertObject(i, obj.type, obj.state, obj.position, false);
			
		}else{
			initWire(obj.fromName, obj.fromOutlet, obj.toName, obj.toOutlet);
		}
	}
	update=true;
}

function nameIsTaken(name){
	if (!localStorage["files"])return false;
	fileArray=JSON.parse(localStorage["files"]);
	return fileArray.hasOwnProperty("file_"+name);
}

function deleteCircuit(name, title){
	if(confirm("Да изтрия ли веригата "+title+"?")){
		fileArray=JSON.parse(localStorage["files"]);
		delete fileArray[name];
		console.log(fileArray);
		localStorage["files"]=JSON.stringify(fileArray);
		listFiles();
		
	}
	
}
function clearCanvas(){
	//Clear everything
    for (var i in wires) 
		deleteWire(i);
			
    for (var i in objects) {
		$("#frame #"+i).remove();
		
	}
	stage.removeAllChildren();
	selectedObjects={};
	selectedWires={};
	objects={};

}