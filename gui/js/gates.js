var templates = new Array();
function loadGates(){
	
	createObjectTemplate("AND", "DEFAULT", {"DEFAULT":"img/gates/AND.png", "HITAREA":"img/gates/AND_hit.png"}, {
		"A": {x: 0, 	y: 20, 		bezierx: -20, 	beziery: 0},
		"B": {x: 0,		y: 65,		bezierx: -20,	beziery: 0},
		"C": {x: 130,	y: 85/2,	bezierx: 20,	beziery: 0}
	}, andOperator);
	createObjectTemplate("NAND", "DEFAULT", {"DEFAULT":"img/gates/NAND.png", "HITAREA":"img/gates/AND_hit.png"}, {
		"A": {x: 0, 	y: 20, 		bezierx: -20, 	beziery: 0},
		"B": {x: 0,		y: 65,		bezierx: -20,	beziery: 0},
		"C": {x: 152,	y: 85/2,	bezierx: 20,	beziery: 0}
	}, nandOperator);
	createObjectTemplate("OR", "DEFAULT", {"DEFAULT":"img/gates/OR.png", "HITAREA":"img/gates/OR_hit.png"}, {
		"A": {x: 0, 	y: 20, 		bezierx: -20, 	beziery: 0},
		"B": {x: 0,		y: 65,		bezierx: -20,	beziery: 0},
		"C": {x: 130,	y: 85/2,	bezierx: 20,	beziery: 0}
	}, orOperator);
	createObjectTemplate("NOR", "DEFAULT", {"DEFAULT":"img/gates/NOR.png", "HITAREA":"img/gates/OR_hit.png"}, {
		"A": {x: 0, 	y: 20, 		bezierx: -20, 	beziery: 0},
		"B": {x: 0,		y: 65,		bezierx: -20,	beziery: 0},
		"C": {x: 150,	y: 85/2,	bezierx: 20,	beziery: 0}
	}, norOperator);
	createObjectTemplate("XOR", "DEFAULT", {"DEFAULT":"img/gates/XOR.png", "HITAREA":"img/gates/XOR_hit.png"}, {
		"A": {x: 0, 	y: 20, 		bezierx: -20, 	beziery: 0},
		"B": {x: 0,		y: 65,		bezierx: -20,	beziery: 0},
		"C": {x: 142,	y: 85/2,	bezierx: 20,	beziery: 0}
	}, xorOperator);
	createObjectTemplate("XNOR", "DEFAULT", {"DEFAULT":"img/gates/XNOR.png", "HITAREA":"img/gates/XOR_hit.png"}, {
		"A": {x: 0, 	y: 20, 		bezierx: -20, 	beziery: 0},
		"B": {x: 0,		y: 65,		bezierx: -20,	beziery: 0},
		"C": {x: 158,	y: 85/2,	bezierx: 20,	beziery: 0}
	}, xnorOperator);
	createObjectTemplate("NOT", "DEFAULT", {"DEFAULT":"img/gates/NOT.png", "HITAREA":"img/gates/NOT_hit.png"}, {
		"A": {x: 0,		y: 85/2,	bezierx: -20,	beziery: 0},
		"B": {x: 135, 	y: 85/2, 	bezierx: 20, 	beziery: 0}
	}, notOperator);
	createObjectTemplate("SWITCH", "ON", {"ON":"img/switch_on.png", "OFF":"img/switch_off.png", "HITAREA":"img/switch_hit.png"}, {
		"STATE": {x: 66,y: 36.5,	bezierx: 20,	beziery: 0}
	}, switchInput);

	templates["SWITCH"].onclick=function(){
		if(this.state=="ON"){
			this.state="OFF";	
		}else{ 
			this.state="ON";

		}
		this.shape.image=templates["SWITCH"].image[this.state];
		this.shape.update=true;
		simulate();
		return true;
	}

	createObjectTemplate("LIGHT", "OFF", {"ON":"img/light_on.png", "OFF":"img/light_off.png", "HITAREA":"img/light_hit.png"}, {
		"STATE": {x: 0,y: 20,	bezierx: -20,	beziery: 0}
	}, lightOutput);


}

function createObjectTemplate(name, defaultState, images, outlets, func){
	templates[name] = new Array();
	templates[name].defaultState=defaultState;
	templates[name].image = new Array();
	for(var state in images){
		templates[name].image[state] = new Image();
		templates[name].image[state].src = images[state];
	}
	
	templates[name].outlets = outlets;
	templates[name].onclick=function(){};
	templates[name].func=func;
}
function andOperator(){
	if (this.outlets["A"].hasValue&&this.outlets["B"].hasValue){
		this.outlets["C"].hasValue=true;
		this.outlets["C"].bit=(this.outlets["A"].bit&&this.outlets["B"].bit);
		this.calculated=true;
	}
}
function nandOperator(){
	if (this.outlets["A"].hasValue&&this.outlets["B"].hasValue){
		this.outlets["C"].hasValue=true;
		this.outlets["C"].bit=(!(this.outlets["A"].bit&&this.outlets["B"].bit));
		this.calculated=true;
	}
}
function orOperator(){
	if (this.outlets["A"].hasValue&&this.outlets["B"].hasValue){
		this.outlets["C"].hasValue=true;
		this.outlets["C"].bit=(this.outlets["A"].bit||this.outlets["B"].bit);
		this.calculated=true;
	}
}
function norOperator(){
	if (this.outlets["A"].hasValue&&this.outlets["B"].hasValue){
		this.outlets["C"].hasValue=true;
		this.outlets["C"].bit=(!(this.outlets["A"].bit||this.outlets["B"].bit));
		this.calculated=true;
	}
}
function xorOperator(){
	if (this.outlets["A"].hasValue&&this.outlets["B"].hasValue){
		this.outlets["C"].hasValue=true;
		this.outlets["C"].bit=(this.outlets["A"].bit!=this.outlets["B"].bit);
		this.calculated=true;
	}
}
function xnorOperator(){
	if (this.outlets["A"].hasValue&&this.outlets["B"].hasValue){
		this.outlets["C"].hasValue=true;
		this.outlets["C"].bit=(!(this.outlets["A"].bit!=this.outlets["B"].bit));
		this.calculated=true;
	}
}
function notOperator(){
	if (this.outlets["A"].hasValue){
		this.outlets["B"].hasValue=true;
		this.outlets["B"].bit=!(this.outlets["A"].bit);
		this.calculated=true;
	}
}
function lightOutput(){
	if(this.outlets["STATE"].hasValue){
		this.state=((this.outlets["STATE"].bit)?"ON":"OFF");
		this.calculated=true;
	}
	this.shape.image=templates["LIGHT"].image[this.state];
	this.update=true;
}
function switchInput(){
	console.log(this.state);
	this.outlets["STATE"].bit=((this.state=="ON")?true:false);
	this.outlets["STATE"].hasValue=true;
	this.update=true;
	this.calculated=true;
}
