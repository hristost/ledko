files={};
function save(){
	file={name:name};
	file.objects={};
	file.wires={};
	for(var objectName in objects){
		object=objects[objectName];
		file.objects[objectName]={type:object.type, position:object.position, state: object.state};
	}
	for(var wireName in wires){
		wire=wires[wireName];
		file.wires[wireName]={fromName:wire.fromName, fromOutlet:wire.fromOutlet, toName:wire.toName, toOutlet:wire.toOutlet};
		
	}
	console.log(file);
}