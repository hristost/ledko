/// Масив, съдържащ описанията на всички компоненти
var componentTemplates = new Array()

var servoGenerateCode = function(obj){
	var connection = getOutletConnection(obj.name, "SIG");
	if (typeof connection === "undefined"){
		return obj.parameterValues.variableName+" = Servo_dummy()";
	}else{
		var connectedObj = objects[connection.object];
		if (connectedObj.type === "MCU"){
			return obj.parameterValues.variableName+" = Servo_controller("+connectedObj.parameterValues.variableName+", "+connection.outlet+")";

		}
	}
}
var ledGenerateCode = function(obj){
	var connection = getOutletConnection(obj.name, "ON");
	if (typeof connection === "undefined"){
		return obj.parameterValues.variableName+" = LED_dummy()";
	}else{
		var connectedObj = objects[connection.object];
		if (connectedObj.type === "MCU"){
			return obj.parameterValues.variableName+" = LED_controller("+connectedObj.parameterValues.variableName+", "+connection.outlet+")";

		}
	}
}
var mcuGenerateCode = function(obj){
	var connection = getOutletConnection(obj.name, "P_I2C");
	if (typeof connection === "undefined"){
		// TODO: Search for serial/USB
		return obj.parameterValues.variableName+" = Controller()";
	}else{
		var connectedObj = objects[connection.object];
		if (connectedObj.type === "BUS_I2C"){	//Излишно, тъй като не са позволени други връзки
			return obj.parameterValues.variableName+" = Controller_i2c("+connectedObj.parameterValues.variableName+", "+obj.parameterValues["address"]+")";

		}
	}
}
var i2cBusGenerateCode = function(obj){
	return obj.parameterValues.variableName+" = smbus.SMBus("+obj.parameterValues["address"]+")";
}
var cameraGenerateCode = function(obj){
	var connection = getOutletConnection(obj.name, "P_CSI");
	if (typeof connection === "undefined"){
		var connection = getOutletConnection(obj.name, "P_USB");
		if (typeof connection === "undefined"){
			return obj.parameterValues.variableName+" = Camera_dummy()";
		}else{
			var connectedObj = objects[connection.object];
			return obj.parameterValues.variableName+" = Camera_usb("+obj.parameterValues["address"]+")";
		}
	}else{
		var connectedObj = objects[connection.object];
		return obj.parameterValues.variableName+" = Camera_raspi("+obj.parameterValues["address"]+")";	
	}
}

var codeGenerationFunctions = {
	"LED": ledGenerateCode,
	"MCU": mcuGenerateCode,
	"CAM": cameraGenerateCode,
	"BUS_I2C": i2cBusGenerateCode,
	"SERVO": servoGenerateCode
}

/// Запълва `componentTemplates`
// TODO: Scan peripherapls folder
function loadComponents() {
	var titleWithAddressFunc = function(object){return object.parameterValues.variableName + '<small style="opacity: 0.5">@' + object.parameterValues["address"]+"</small>"};

	createComponentTemplate("LED", "LED", "Едноцветен светодиод", "./img/LED_icon.png", "output", [
		outlet("ВКЛ.",'L', 'I', "ON")
		]);
	createComponentTemplate("MOT", "Мотор", "DC Електромотор. Необходим е драйвер/H-Bridge за управление.", "./img/Motor_icon.png", "output", [
		outlet("УПР.", 'L', 'МОТ', "ON")
		]);
	createComponentTemplate("SERVO", "Серво", "Серво мотор", "./img/Servo_icon.png", "output", [
		outlet("УПР.", 'L', 'I', "SIG")
		]);
	createComponentTemplate("MCU", "Микроконтролер", "Микроконтролер", "./img/Chip_small_icon.png", "", [
		outlet("USB", 'L', 'USB', "P_USB"),
		outlet("i2c", 'L', 'i2c', "P_I2C"),
		outlet("Serial", 'L', 'Serial', "P_SER"),
		outlet("0", 'R', 'IO', 0),
		outlet("1", 'R', 'IO', 1),
		outlet("2", 'R', 'IO', 2),
		outlet("3", 'R', 'IO', 3),
		outlet("4", 'R', 'IO', 4),
		outlet("5", 'R', 'IO', 5),
		outlet("6", 'R', 'IO', 6),
		outlet("7", 'R', 'IO', 7),
		outlet("8", 'R', 'IO', 8),
		outlet("9", 'R', 'IO', 9),
		outlet("10", 'R', 'IO', 10),
		outlet("11", 'R', 'IO', 11),
		outlet("12", 'R', 'IO', 12),
		outlet("13", 'R', 'IO', 13),
		outlet("A0", 'R', 'IO', 14),
		outlet("A1", 'R', 'IO', 15),
		outlet("A2", 'R', 'IO', 16),
		outlet("A3", 'R', 'IO', 17),
		outlet("A4", 'R', 'IO', 18),
		outlet("A5", 'R', 'IO', 19)
		], [parameter("address", "Адрес", "AUTO")], titleWithAddressFunc);
	createComponentTemplate("H_BRIDGE", "H-Мост", "Драйвер за DC електромотор.", "./img/HBridge_icon.png", "", [
		outlet("MOT.", 'R', 'MOT', "MOT"),
		outlet("DIR.", 'L', 'I', "A"),
		outlet("PWM", 'L', 'I', "PWM"),
		outlet("EN.", 'L', 'I', "EN")
	]);
createComponentTemplate("CAM", "Камера", "Камера", "./img/Camera_icon.png", "input", [
		outlet("USB",'L', 'USB', "P_USB"),
		outlet("CSI",'L', 'CSI', "P_CSI")
		], [parameter("address", "Адрес", 1)], titleWithAddressFunc);
	createComponentTemplate("BUS_I2C", "i2c", "i2c шина", "./img/I2cCable_icon32.png", "", [
		outlet("i2c", 'R', 'i2c', 0),outlet("i2c", 'R', 'i2c', 1),outlet("i2c", 'R', 'i2c', 2),outlet("i2c", 'R', 'i2c', 3),outlet("i2c", 'R', 'i2c', 4),outlet("i2c", 'R', 'i2c', 5),outlet("i2c", 'R', 'i2c', 6), outlet("i2c", 'R', 'i2c', 7)
		], [parameter("address", "Адрес", 1)], titleWithAddressFunc);
	createComponentTemplate("BUS_USB", "USB", "USB порт", "./img/USB_icon32.png", "", [
		outlet("USB", 'R', 'USB', 0),outlet("USB", 'R', 'USB', 1),outlet("USB", 'R', 'USB', 2),outlet("USB", 'R', 'USB', 3)
		]);
}

/// Дефинира извод на компонент
///
/// :params:
/// outletName Име на извода
/// outletPosition От коя страна на компонента се намира - лява ('L') или дясна ('R')
/// outletType Типа на извода - I, O, IO, MOT, P_USB, P_I2C, P_SERIAL
function outlet(outletName, outletPosition, outletType, outletPin){
	return {name: outletName, pin: outletPin, position: outletPosition, type: outletType}
}

/// Дефинира параметър на компонент
function parameter(parameterKey, parameterName, parameterDefaultValue){
	return {name: parameterName, param: parameterKey, defaultValue: parameterDefaultValue, type: "TEXT"}
}

/// Дефинира компонент
function createComponentTemplate(componentType, componentTitle, componentDescription, componentImage, componentClasses, componentOutlets, componentParameters, titleFunction){
	componentTemplates[componentType] = {};
	componentTemplates[componentType].title = componentTitle;
	componentTemplates[componentType].type = componentType;
	componentTemplates[componentType].description = componentDescription;
	componentTemplates[componentType].image = componentImage;
	componentTemplates[componentType].classes = componentClasses;
	componentTemplates[componentType].outlets = componentOutlets;
	componentTemplates[componentType].parameterValues = {};

	// Ако няма зададени параметри, включи само името на променливата
	if (typeof componentParameters === "undefined")
		componentTemplates[componentType].parameters = [parameter("variableName", "Име")];
	else
		componentTemplates[componentType].parameters = [parameter("variableName", "Име")].concat(componentParameters);
	var params = componentTemplates[componentType].parameters;
	for (i=0; i<params.length; i++){
		var param = params[i];
		componentTemplates[componentType].parameterValues[param.param] = param.defaultValue;
	}

	if (typeof titleFunction === "undefined")
		componentTemplates[componentType].titleFunction = function(object){return object.parameterValues.variableName}
	else
		componentTemplates[componentType].titleFunction = titleFunction
}

/// Масив с параметрите на `component`
function getComponentParameters(component){
	var params = {};
			console.log("getP")

	for (i=0; i<component.parameters.length; i++){
		var param = component.parameters[i];
		console.log(param.param)
		params[param.param] = component[param.param];
	}
	return params;
}

function getOutletConnection(componentName, componentOutlet){
	for (i in wires){
		var wire = wires[i];
	if (wire.fromName == componentName && wire.fromOutlet == componentOutlet){
		return {object: wire.toName, outlet: wire.toOutlet}
	}else if (wire.toName == componentName && wire.toOutlet == componentOutlet){
		return {object: wire.fromName, outlet: wire.fromOutlet}
	}
	}
}

/// Координатите на даден компонент
function getComponentPosition(component){
	return $("#frame #"+component.name).offset();
}
/// Създава HTML код за визуализирането на компонента `component`. Събития за взаимодействие с потребителя не се добавят тук.
function createComponentHTML(component){
	var html = '<div id="'+component.name+'" class = "component '+component.classes+'"><table class="component">';

	//Раздели изходите на лява и дясна страна
	var leftOutlets = [];
	var rightOutlets = [];
	for (i=0; i<component.outlets.length; i++){	
		var outlet = component.outlets[i];
		if (outlet.position == 'R')
			rightOutlets.push(outlet);
		else
			leftOutlets.push(outlet);	
	}

	// Разположи изходите и картинката в таблица
	var tableRows = Math.max(leftOutlets.length, rightOutlets.length);
	for (i=0; i<tableRows; i++){
		html += "<tr>";
		var leftOutletCell = "";
		var rightOutletCell = "";

		if (leftOutlets.length > 0)
			if (i<leftOutlets.length)
				leftOutletCell = '<td data-outlet="'+leftOutlets[i].pin+'" data-outlet-type="'+leftOutlets[i].type+'"> <div></div>'+leftOutlets[i].name+"</td>";
			else
				leftOutletCell =  "<td></td>";
		
		// Аналогично за изводите от дясната страна
		if (rightOutlets.length > 0)
			if (i<rightOutlets.length)
				rightOutletCell = '<td class="right" data-outlet="'+rightOutlets[i].pin+'" data-outlet-type="'+rightOutlets[i].type+'">'+rightOutlets[i].name+'<div></div></td>';
			else
				rightOutletCell =  "<td></td>";
		
		// Картинката обхваща всички редове с rowSpan, затова се поставя само на първи ред
		var imageCell = (i==0) ? "<td rowspan=" + tableRows +'><img src="'+component.image+'"></img></td>' : ""
		
		html += leftOutletCell + imageCell + rightOutletCell;
		html += "</tr>";
	}
	html += '</table><footer><span data-variable class="parametersLink">'+component.titleFunction(component)+'</span>';
	html += '<a class="parametersButton" onclick="openParametersPane('+"'"+component.name+"'"+');"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></a></footer>';
	html += '<div class="parameterPane"></div>'
	html += "</div>";

	return html;
}

/// Създава HTML таблица, която съдържа полета за всички параметри на компонента `component`
function createComponentSettingsPanelHTML(component){
	var html = "<table>";
	for (i=0; i<component.parameters.length; i++){	
		var param = component.parameters[i];
		html += "<tr>";
		html += "<td>" + param.name + '</td><td><input data-bound-parameter="'+param.param+'" data-bound-object="'+component.name+'" value="'+component.parameterValues[param.param]+'"></input></td>';
		html += "</tr>";
	}
	html += "</table>";
	html += '<div class="btn-group" role="group" aria-label="...">'
	html += '<button type="button" class="btn btn-default btn-s" onclick="closeParametersPane('+"'"+component.name+"'"+');">Откажи</button>'
	html += '<button type="button" class="btn btn-primary btn-s" onclick="submitParametersPane('+"'"+component.name+"'"+');">Промяна</button>'
	html += '</div>'
	return html;
}